// main.js: main setup and c
// setup menu and templates
/*jslint node: true, evil: false, newcap: true, sub: true, vars: true, indent: 4, nomen: true, plusplus: true*/
/*global angular, $, Handlebars, window*/
(function () {
    "use strict";

    var gPAGE_OFFSET = 50;  // page offset from top of viewport

    // navigation event callbacks for menu click and scroll
    // to sync navbar with click/scroll events
    function setupNav() {

        var $nav = $('.navbar'),
            $parent = $('nav').parent(), // width based on header width
            sClass = 'nav-fixed-scroll';

        // helper: nav active class
        function updateActiveMenuClass(anchor) {
            $('a').each(function () {
                $(this).removeClass('active');
            });
            $(anchor).addClass('active');
        }

        // scroll and resize callback
        // - fix menu to top based on scrolling past the header
        // - sync the active menu item with the current scroll position
        var manageNav = function () {
            var scrollPosition = $(this).scrollTop();
            if (((scrollPosition >= $nav.offset().top)) &&
                    (scrollPosition !== 0)) {
                //console.log('manageNav: adding nav fixed');
                $nav.addClass(sClass);
            } else {
                //console.log('manageNav: removing nav fixed');
                $nav.removeClass(sClass);
            }
            $nav.width($parent.width());

            // keep navbar synced with active menu item class on window scroll
            $('nav a').each(function () {
                var currentLink = $(this),
                    refElement = $(currentLink.attr("href")),
                    refTop = refElement.position().top,
                    refHeight = refElement.height() || 0,
                    elementBottom = refTop + refHeight,
                    topOfPage = elementBottom ? false : true;

                if ((refTop - gPAGE_OFFSET <= scrollPosition) && (elementBottom > scrollPosition)) {
                    $('a[href^="#"]').removeClass("active");
                    currentLink.addClass("active");
                    //console.log('Click cb: add active menu class for ' + currentLink.html());
                } else {
                    currentLink.removeClass("active");
                    //console.log('Click cb: remove active menu class for ' + currentLink.html());
                }
            });
        };

        // nav menu click handler
        $('a[href^="#"]').on('click', function (e) {

            // handle the click here
            e.preventDefault();
            $('#navbar.collapse.in').collapse('hide');

            $(window).off("scroll");
            updateActiveMenuClass(this);

            var target = this.hash,
                $target = $(target);

            $('html, body').stop().animate({
                'scrollTop': $target.offset().top

            }, 500, 'swing', function () {
                window.location.hash = target;
                $.call(this, manageNav);
                $(window).on("scroll", manageNav);
            });
        });

        // print click handler
        $('nav li.printLink').on('click', function (e) {

            e.preventDefault();
            window.print();
        });

        // init menu active class for load
        updateActiveMenuClass($("a[href='#resume']")[0]);

        // return cb for scroll and menu click
        return manageNav;
    }

    // internal cache for external templates for education and
    // experience templates & data.
    var _hbTemplateCache;
    function loadExternalTemplate(name) {

        var fname_base = 'templates/' + name,

            promise = $.Deferred(),

            readFile = function (url, dataType) {
                return ($.ajax({
                    url: url,
                    dataType: dataType
                }));
            },

            templateResponse = readFile(fname_base + ".handlebars", "text"),

            contextResponse = readFile(fname_base + ".json", "json");


        $.when(templateResponse, contextResponse)
            .done(function () {
                Handlebars.registerHelper('eachArray', function (context, options) {
                    var ret = "",
                        wrapTag = options.hash.wrapTag || "span";

                    var i, j;
                    for (i = 0, j = context.length; i < j; i++) {
                        ret += '<' + wrapTag + '>' + context[i] +
                                '</' + wrapTag + '>';
                    }

                    return ret;
                });
                var template = templateResponse.responseText,
                    context = JSON.parse(contextResponse.responseText);

                if (_hbTemplateCache === undefined) {
                    _hbTemplateCache = {};
                }
                _hbTemplateCache[name] = template;
                promise.resolve({template: template,
                          context: context});
            })

            .fail(function (err) {
                console.error(name + " template and json data not loaded.");
            });

        return promise;
    }

    // Handlebars helper: inner array wrapped with spans
    function setupHandlebars() {

        // #eachArray:  array output with items wrapped in
        // specified element [span = default]
        //
        // use: {#eachArray name htmlElementWrap}
        //
        Handlebars.registerHelper('eachArray', function (context, options) {
            var ret = "",
                wrapTag = options.hash.wrapTag || "span";

            var i, j;
            for (i = 0, j = context.length; i < j; i++) {
                ret += '<' + wrapTag + '>' + context[i] +
                        '</' + wrapTag + '>';
            }

            return ret;
        });
    }

    function setupTemplates() {
        var loadTemplate = function (name, container) {
            loadExternalTemplate(name).done(function (response) {
                var compile = Handlebars.compile(response.template),
                    html = compile(response.context);
                $(container).append(html);
            });
        };

        loadTemplate('jobs', '#jobsContainer');
        loadTemplate('edu', '#edContainer');
    }

    /// setup app ///
    var manageNav = setupNav(); // set callback for scroll events
    setupHandlebars();
    setupTemplates();

    // manage events
    $(window).scroll(manageNav);
    $(window).resize(manageNav);

}());
