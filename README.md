# Liz McMahon | Resume
This code generates my online _responsive_ resume built with gulp and implemented using LESS and Bootstrap with automatic file watch, lint and minification.

## Resume views

**Initial**
<img src="http://thequarrygroup.com/resume/assets/media/screenshots/resume.png" />

######Header is in viewport: the navigation will scroll until the header is no longer visible. Shows the active and hover menu item style.

**Scrolled**
<img src="http://thequarrygroup.com/resume/assets/media/screenshots/resume-pinned.png" />

######Fixed Navigation after the header has scrolled off the viewport.

## Basic File Structure

The project structure is:

 * index.htm
 
 * /assets
    * /js : main.js **[ navigation, scrolling and handlebars templates processing ]**
        * /vendor: bootstrap, handlebars and modernizer js files
    * /less : resume.less, bootstrap and style.less (helper styles)
    * /lib : current jquery .js and .min.js files for dev
    * /media : page bullet images
	    * /docs : current resume (.doc, .pdf) and Open Office Writer source

 * /build
    * jquery.plugins.min.js [**gulp squishjs**: plugins & vendor files from /assets/js/**/ ]
    * jquery-1.11.2.min.js
    * resume.css - [**gulp build-css**: compiles /assets/*.less files. 
    * scripts.js - [**gulp build-js**: customized minified and uglified /assets/js/*.js files (main.js will be there, currently still using DEV mode).

 * /css - bootstrap & bootstrap-templates minified css files

 * /fonts - glyphicons

 * /node_modules - gulp build files

## Features

This resume was generated using Handlebars with job and education listing templates. The styles were defined with Bootstrap and a customized LESS css file.

- Bootstrap 3.0.
- Features a scrolling navigation that can be transformed to fixed (locked at the top of the screen). When the page is scrolled past the header, the scrollbar will **_BOOM_** pin it right at the top. A Main.js callback monitors the scroll and resize events to ensure that:
	+ the navigation's active menu item is automatically set when scrolled in that area
	+ the navigation width is adjusted on resize. Because this resizing involves a fluid with component,  JS was used to set the width based on the relative parent's width 
- The navigation menu is to quickly drop down to sections of interest.
- Print functionality and print-only styles to optimize print display.
- SVG was utilized for simple bullets and line draws.
- Handlebars was used to enable the job and education objects to be defined outside the html to simplify maintenance and the task of loading the repeated html format for their display. Containers were declared in index.htm, and populated using seperate, external Handlebar template file and external JSON data.
- Generation of a new handlebars helper function to output the array of job tags and the course listings within the current listing iteration.
- LESS CSS generation to streamline the development of the styles used in the responsive display

<img src="http://thequarrygroup.com/resume/assets/media/screenshots/mobile-view.png">

######Mobile View

### More on Responsive, LESS on CSS Stuff
- Watches for Less changes on save
- Checks for Less errors and outputs them without you having to rerun Gulp
- Auto prefixes for legacy browsers
- Combines all CSS into one big minified file (eventually... currently I'm developing, so I want to expose the styles for debug).
- Includes Less Bootstrap

### JavaScript Stuff
- Automatically compiles all jQuery libraries into one big, minified, happening JS file
- Lints custom scripts for errors
- Combines all custom scripts into one file

### Building flow
- @ the start of the day: ```$ gulp watch```
- update the custom css: ```$gulp build-css```
- update custom js (current don't need to do this, using the main.js source for Dev) ```$gulp build-js```
- New vendor (outside resource) js file ```$gulp squish-jslib```

#### Quick Tips
- Any changes in `assets/less/*` will trigger the Less to compile on save
- All files in `assets/js/libs/*` will be compressed to `build/jquery.plugins.min.js`
- All files in `assets/js/*` (except for `libs`) will be compressed to scripts.min.js

### Template Stuff
Handlebars.js handles the templating of the *Experience* and *Education* sections of the document. The templates and associated json files are located in the /templates directory. This separates the main html, section layouts and data quite effectively. A quick screen shot shows the template and data files for the Experience portion of the document.

<img src="http://thequarrygroup.com/resume/assets/media/screenshots/template-code-view.png" />

###### Left Pane: json template to populate the main document. Handlebars iterates over the json objects to generate the html for the section.
###### Right Pane: json data for the job description, company information and skills utilized.

These files are included asynchronously in the main document, via javascript, greatly reducing the amount of html in the main page if included as scripts, or the external js if defined there. 

```HTML
	<section id="experience" class="col-md-12">
	    <h2>Experience</h2>
	    <div id="jobsContainer"></div>
	</section>
	<section  id="education" class="ed col-md-12">
	    <h2>Education</h2>
	    <div id="edContainer"></div>
	</section>
```

#### Special Thanks

To https://github.com/scotch-io/ for the starter gulp/less framework...*makes building a breeze* and all the open source code available to make software fun!
